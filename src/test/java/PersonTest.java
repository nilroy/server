import org.junit.Assert;
import org.junit.Test;

public class PersonTest {
    @Test
    public void testFullName(){
        Person nilroy = new Person("Niladri", "Roy");
        Assert.assertEquals("Niladri Roy", "Niladri Roy");
    }
}
